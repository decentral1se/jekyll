---
layout: post
title:  "Welcome to meet.coop"
date:   2020-05-08 10:30:00 +0100
categories: news
published: false
---

Yesterday [Webarchitects](https://www.webarchitects.coop) registered `meet.coop` on behalf of [The Online Meeting Co-operative](/about/) and we hope to be able to bring you more news about our plans over the next few days.
