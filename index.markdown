---
layout: default
title: Open source cooperative video conference, meet.coop
---

**The Online Meeting Co-operative is working to provide an open source, meeting and conferencing platform, powered by [BigBlueButton](https://bigbluebutton.org/).**

This project was formed by [DigidemLab](https://digidemlab.org/en/) / [Collective.tools](https://collective.tools/), [femProcomuns](https://femprocomuns.coop/) / [CommonsCloud](https://www.commonscloud.coop/) and [Webarchitects](https://www.webarchitects.coop/), and several other actors of the Commons and Social and Solidarity Economy are in conversations to join this collective effort. Organisations can [get involved and become a member](/getinvolved.html) if you are willing to contribute time and/or money to the shared objective. Contact [contact@meet.coop](mailto:contact@meet.coop).

You can make use of the shared services and contribute to its sustainability as follows:

1. **Participant:** register a free account and join an online session. We encourage you to make a donation through LiberaPay to the project and session organisers
2. **Member organisation:** Commons and/or Social and Solidarity Economy organisation committed to contribute an annual membership in the form of time and/or money valued at 500 - 1000€. After start up, membership contributions for the second year are expected to be lower.
3. **Session Organiser:** someone who works at or is a member of one of the member organisations with rights to start and record sessions
4. **Assembly:** each member organisations has one vote in the Assembly
5. **DevOps:** those member organisations willing to contribute time to development and run the operation are invited to join the DevOps team

## Shared values

Our work is in the Social and Solidarity Economy, we abide by the 7 cooperativist principles:

1. **Voluntary and Open Membership**
We encourage organisations from the Commons and Social and Solidarity Economy to join as members to make use of the shared resource.
2. **Democratic Member Control**
Members have equal rights in the governance: one member equals one vote. Information about the project is shared transparently between members.
3. **Member Economic Participation**
Members are expected to contribute an equal share to the collective costs of running the project. For the first year higher costs are expected to develop the services and organisations are expected to contribute 20-40 hours of work according to the agreed needs and/or 500-1000€ to share the costs of servers. Different contributions can be made by non-members, be they individuals or organisational users who contribute voluntarily (donation) according to their usage of the common services.
4. **Autonomy and Independence**
Initially the online meeting cooperative is an informal organisation formed by the member coops that have started the project. Built on mutual trust and intercooperation agreements, the project seeks to be a self-managed, autonomous organisation. Domains, assets and other shared resources will be transfered to a future independent legal entity, once the partner organisations decide to create this.
5. **Education, Training, and Information**
A key objective of the project is to share knowledge and experience about running the shared services and facilitate its replication, though internal training, documentation, deplayment scripts and a culture of sharing knowledge and software under free licenses with collaborative methodologies.
6. **Cooperation among Cooperatives**
The project is started by three cooperative, Collective.tools, femProcomuns and Webarchitects and other cooperative actors who seek to strengthen cooperation among themselves to enrich and strengthen the Social and Solidarity Economy.
7. **Concern for Community**
A driving force for this project is our care for privacy and Free Software, to allow people and organisations to have quality online meetings with ethical tools. The project has at its centre the community resource of online meeting services that are shared as a commons between its members. Its an open commons as social and solidarity economy organisations can join the cooperative and individual people are able to join through (some of) the member organisations. Furthermore, the generated software scripts and knowledge are shared as Free Software and under free licenses, following collaborative methodologies that facilitate replication and a decentralised architecture. 
