# The Online Meeting Co-operative 

[![pipeline status](https://git.coop/meet/jekyll/badges/master/pipeline.svg)](https://git.coop/meet/jekyll/-/commits/master)

This repo contains the [Jekyll](https://jekyllrb.com/) code for the
[www.org.meet.coop](https://www.org.meet.coop/) production website and also the
staging website at [www.stage.org.meet.coop](https://www.stage.org.meet.coop/).

All commits to the master branch are automatically built using a [custom Docker
container](https://git.coop/webarch/containers#jekyll) and automatically
deployed to the [staging site](https://www.stage.org.meet.coop/) where the
changes can be checked and if there are no mistakes then a manual action can be
triggered to update the [live site](https://www.org.meet.coop/), see the
[pipelines page](https://git.coop/meet/jekyll/pipelines) and the
`deploy:production` button:

![Deploy pipeline screenshot](pipeline.png)


